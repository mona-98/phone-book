import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditUserComponent } from './user-detail/edit-user/edit-user.component';
import { ViewUserComponent } from './user-detail/view-user/view-user.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
const routes: Routes = [
  { path: 'edit/:id', component: EditUserComponent },
  { path: 'view', component: ViewUserComponent },
  { path :'',  component:UserDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
