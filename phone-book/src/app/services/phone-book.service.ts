import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PhoneBookService {

  constructor(private http:HttpClient) { }

  insertData(data) {
     return this.http.post("http://localhost:3000/insert",data);
  }

  readData(data) {
    return this.http.post("http://localhost:3000/read",data);
  }

  updateData(data) {
    return this.http.post("http://localhost:3000/update",data);
  }

  deleteData(data) {
    return this.http.post("http://localhost:3000/delete", data);
  }
}
