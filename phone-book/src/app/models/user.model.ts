export class User {
    id:string;
    name:string;
    phoneNo:number;
    alternativePhoneNo:number;
    email:string;
}