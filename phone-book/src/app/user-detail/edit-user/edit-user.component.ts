import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { PhoneBookService } from 'src/app/services/phone-book.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  userModel: User;
   id;
   isNew: boolean = false;
   photo: File = null;
   expPhoto: any;
   
  constructor(private service: PhoneBookService,private _Activatedroute:ActivatedRoute, private route: Router) { }

  ngOnInit() {
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
    this.isNew = (this.id === 'new');
    this.userModel=new User();
    var self=this;
    this.service.readData(this.id).subscribe((data:any)=> {
      for(var i=0;i<data.data.length;i++) {
        
        if( data.data[i].id == this.id) {
          this.userModel=data.data[i];
        }
      }

  });
  }

  onSubmit() {
    let reqData = {
      user: this.userModel
    };
    if (this.isNew) {
       this.service.insertData(reqData).subscribe((data: any) => {
         if (data.status === 'SUCCESS') {
          alert('Successfully added');
           this.navigateToUserDetail();
         } else {
          alert('Add failed: ' + data.message);
         }
       });
    }
    else {
      this.service.updateData(reqData).subscribe((data: any) => {
        if (data.status === 'SUCCESS') {
          alert('Successfully edited');
          this.navigateToUserDetail();
        } else {
          alert('Edit failed: ' + data.message)
        }
      });
    }
  }

  delete() {
    let reqData = {
      id: this.userModel.id
    };
    if (!this.isNew) {
      this.service.deleteData(reqData).subscribe((data: any) => {
        if (data.status === 'SUCCESS') {
          alert('Successfully deleted');
          this.navigateToUserDetail();
        } else {
          alert('Delete failed: ' + data.message);
        }
      })
    }
  }

  navigateToUserDetail() {
    this.route.navigate(['/']);
  }

  addPhoto(event: any) {
    let files = event.target.files;
    if (files && files.length && (files.length > 0)) {
      this.photo = files[0];
      let reader: FileReader = new FileReader();
      reader.readAsDataURL(this.photo);
      reader.onload = () => {
        this.expPhoto = reader.result;
      }
    } else {
      this.photo = null;
      this.expPhoto = null;
    }
  }
}
