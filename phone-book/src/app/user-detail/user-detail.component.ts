import { Component, OnInit } from '@angular/core';
import { PhoneBookService } from '../services/phone-book.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  providers:[PhoneBookService]
})
export class UserDetailComponent implements OnInit {
  data=[];
  columnNames = ['name', 'phoneNo', 'email', 'alternativePhoneNo'];
  columnDisplayNames = ['Name', 'Phone number', 'Email', 'Alternative phone number'];
  searchData: any[] = [];
  sortColumn:string='name';
  isAsc:boolean=true;

  constructor(private service:PhoneBookService,private router:Router) { }

  ngOnInit() {
    for (let col of this.columnNames) {
      this.searchData.push({
        name: col,
        value: undefined
      });
    }
    this.service.readData(this.data).subscribe((data:any)=> {
        this.data=data.data
    });
  }

  sort (value) {
      if(this.sortColumn===value){
        this.isAsc=!this.isAsc;
      }
      else {
        this.sortColumn=value;
        this.isAsc=true;
      }
  }

  search (colName, event) {
    let temp: any[] = [];
    for (let searchColumn of this.searchData) {
      if ((searchColumn.name !== colName) && searchColumn.value) {
        temp.push(searchColumn);
      }
    }
    temp.push({
      name: colName,
      value: event.target.value
    });
    this.searchData = temp;
  }

  edit(value) {
    this.router.navigate(['/edit/' + value.id])
  }
  addNewItem() {
    this.router.navigate(['/edit/' + "new"]);
  }

}
