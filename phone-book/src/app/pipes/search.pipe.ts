import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
 
  transform(data:User[], searchData:any[]): any {
    for (let searchColumn of searchData) {
      if (searchColumn.value) {
        data = data.filter((item: any) => {
          item[searchColumn.name] = item[searchColumn.name].toString();
          return (item[searchColumn.name].indexOf(searchColumn.value) !== -1);
        });
      }
    }
    return data;
  }

}
