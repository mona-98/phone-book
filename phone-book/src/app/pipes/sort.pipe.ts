import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user.model';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(items:User[], sortColumn: string, isAsc:boolean): any {
    let f=1;
    if(!isAsc) {
      f=-1;
    }
    return items.sort((a, b) => {
      a[sortColumn]=a[sortColumn].toString();
      b[sortColumn]=b[sortColumn].toString();
      if (a[sortColumn] < b[sortColumn]) {
          return -1*f;
      } else if (a[sortColumn] > b[sortColumn]) {
          return 1*f;
      } else {
          return 0;
      }
  });
  }

}
