const fs = require('fs');
const path = require('path');
const config = {
    location: path.resolve(__dirname, './../datastore/config.json')
};

function getNewId(callback) {
    fs.readFile(config.location, (err, data) => {
        if (err) {
            callback(-1);
        } else {
            let configJson = JSON.parse(data);
            configJson.id = configJson.id + 1;
            fs.writeFile(config.location, JSON.stringify(configJson), (err) => {
                if (err) {
                    callback(-1);
                } else {
                    callback(configJson.id);
                }
            });
        }
    });
}

module.exports = { getNewId };
