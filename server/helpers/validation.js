function validateDataWithKey(data, keys) {
    if (!data || !keys) {
        return false;
    }
    for (let key of keys) {
        if (!data[key]) {
            return false;
        }
    }
    return true;
}

module.exports = { validateDataWithKey };
