const express = require('express');
const app = express();
const port = 3000;
const cors = require ('cors');
const fs = require('fs');

app.use(express.json());
app.use(cors());

const datastore = {
    location: './datastore/phone-book.json',
    keys: ['id', 'name', 'phoneNo', 'email', 'alternativePhoneNo']
};
const {getNewId }= require('./helpers/configuration');

app.post('/insert',(req,res) => {
    let reqData = req.body;
    let user = reqData.user;
    fs.readFile(datastore.location, (err, data) => {
        if (err) {
            res.send(JSON.stringify({
                message: 'error while read file', status: 'FAILURE'
            }));
        } else {
            let dataStore = JSON.parse(data);
            getNewId((newId) => {
                if (newId === -1) {
                    res.send(JSON.stringify({
                        message: 'error while generating new id', status: 'FAILURE'
                    }));
                } else {
                    user.id = newId;
                    dataStore.push(user);
                    fs.writeFile(datastore.location, JSON.stringify(dataStore), (err) => {
                        if (err) {
                            res.send(JSON.stringify({
                                message: 'error while write file', status: 'FAILURE'
                            }));
                        } else {
                            res.send(JSON.stringify({
                                message: 'successfully inserted', status: 'SUCCESS'
                            }));
                        }
                    });
                }
            });
        }
    });
});

app.post('/read', (req, res) => {
    fs.readFile(datastore.location, (err, data) => {
        if (err) {
            res.send(JSON.stringify({
                dataAvailable: false
            }));
        } else {
            let dataStore = JSON.parse(data);
            let resData = {
                dataAvailable: true,
                data: dataStore
            };
            res.send(JSON.stringify(resData));
        }
    });
});

app.post('/update', (req, res) => {
    let user = req.body.user;
    fs.readFile(datastore.location, (err, data) => {
        if (err) {
            res.send(JSON.stringify({
                message: 'error while updating', status: 'FAILURE'
            }));
        } else {
            let dataStore = JSON.parse(data);
            for (let i = 0; i < dataStore.length; i++) {
                if (dataStore[i].id === user.id) {
                    dataStore[i] = user;
                    break;
                }
            }
            fs.writeFile(datastore.location, JSON.stringify(dataStore), (err) => {
                if (err) {
                    res.send(JSON.stringify({
                        message: 'error while write file', status: 'FAILURE'
                    }));
                } else {
                    res.send(JSON.stringify({
                        message: 'successfully updated', status: 'SUCCESS'
                    }));
                }
            })
        }
    })
});

app.post('/delete', (req, res) => {
    let id = req.body.id;
    fs.readFile(datastore.location, (err, data) => {
        if (err) {
            res.send(JSON.stringify({
                message: 'error while updating', status: 'FAILURE'
            }));
        } else {
            let dataStore = JSON.parse(data);
            let remIdx = dataStore.findIndex((item) => {
                return (item.id == id);
            });
            if (remIdx !== -1) {
                dataStore.splice(remIdx, 1);
                fs.writeFile(datastore.location, JSON.stringify(dataStore), (err) => {
                    if (err) {
                        res.send(JSON.stringify({
                            message: 'error while write file', status: 'FAILURE'
                        }));
                    } else {
                        res.send(JSON.stringify({
                            message: 'successfully deleted', status: 'SUCCESS'
                        }));
                    }
                });
            } else {
                res.send(JSON.stringify({
                    message: 'id not found', status: 'FAILURE'
                }));
            }
        }
    });
});

app.listen(port,()=> {
    console.log("port is listening at",port);
});
